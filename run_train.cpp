#include <ExtremeValueMachine.hpp>
#include <boost/algorithm/string.hpp>
#include <cstddef>
#include <fstream>
#include<vector>
#include<iostream>

using namespace std;

#define OPEN_MP_SUPPORT true

void k_fold_cv_split(vector<vector<double> > & data, vector<string> & labels,vector<vector<vector<double> > > & split_data, vector<vector<string> > & split_labels,int k);

/**
 * Returns the percentage accuracy of the predictions 
 */
double evaluate_results(vector<string> predictions,vector<string> test_labels){
  double num_correct = 0.0;
  for (int i=0; i<predictions.size();i++){
    if (predictions[i].compare(test_labels[i]) == 0.0){
      num_correct++;
    }
  }
  double result = 100.0*num_correct/((double)test_labels.size());
  cout << "Results on LETTER dataset: " << 100.0*num_correct/((double)test_labels.size()) << " % accuracy." << endl;
  return result;
}

//read generic dataset
void readDataset(string &path,vector<string> & labels, vector<vector<double> > & data){
  cout << "reading dataset from file " << path << endl;
  ifstream file(path.c_str());
  string line;
  while(!file.eof()){
    getline(file,line);
    vector<string> split_strs;
    boost::split(split_strs,line,boost::is_any_of(","));
    labels.push_back(split_strs[0]);
    vector<double> feat_vec;
    for (int i=1;i<split_strs.size();i++){
      feat_vec.push_back(stod(split_strs[i],NULL));
    }
    data.push_back(feat_vec);
  }
}

//splits the dataset up into k folds; default is 10
//We don't do a good job of fold balancing at the moment b/c none of our datasets are balanced in this way
void k_fold_cv_split(vector<vector<double> > & data, vector<string> & labels,
		     vector<vector<vector<double> > > & split_data, vector<vector<string> > & split_labels,
		     int k){
  int N = data.size();
  int n_data_per_fold = N/k;
  for (int i=0;i<k;++i){
    vector<vector<double> > temp_data(data.begin()+(i*n_data_per_fold),data.begin()+(i+1)*n_data_per_fold);
    vector<string> temp_labels(labels.begin()+(i*n_data_per_fold),labels.begin()+(i+1)*n_data_per_fold);
    split_data.push_back(temp_data);
    split_labels.push_back(temp_labels);
  }
  for (int i=n_data_per_fold*k;i<N;++i){
    split_data[k-1].push_back(data[i]);
    split_labels[k-1].push_back(labels[i]);
  }
}
double run_one_fold(const vector<vector<vector<double> > > & split_data, 
		    const vector<vector<string> > & split_labels,
		    const int foldID,
		    const int k,
		    const double C,
		    const int max_tailsize){
  //build the test set
  vector<vector<double> > test_data(split_data[foldID].begin(),split_data[foldID].end());    
  vector<string> test_labels(split_labels[foldID].begin(),split_labels[foldID].end());
  //build the train set
  vector<vector<double> > train_data;
  vector<string> train_labels;
  for (int train_fold = 0;train_fold<k;++train_fold){
    if (train_fold == foldID){
      continue;
    }
    train_data.insert(train_data.end(),split_data[train_fold].begin(),split_data[train_fold].end());
    train_labels.insert(train_labels.end(),split_labels[train_fold].begin(),split_labels[train_fold].end());
  }

  double lambda = 1.0;
  int min_tailsize = 5;
  double margin_prob_thresh= 0.005;
  double cover_prob_thresh = 0.5;
  double sliding_window_max = 20;
  EVM * evm = new EVM(lambda, 
		      min_tailsize, 
		      max_tailsize, 
		      margin_prob_thresh, 
		      cover_prob_thresh, 
		      sliding_window_max);

  cout << "Fitting EVM for fold " << foldID << "..." << endl;
  evm->fit(train_data,train_labels);
  cout << "done" << endl;

  cout << "Running prediction for fold " << foldID << endl;
  vector<string> predictions = evm->predict(test_data);
  cout << "done" << endl;
  delete evm;
  double result = evaluate_results(predictions,test_labels);
  cout << "fold " << foldID << " completed." << endl;
  return result;
}


void run_k_fold_cv(const string & outFilename,
		   const vector<vector<vector<double> > > & split_data, 
		   const vector<vector<string> > & split_labels,
		   const vector<double> C_vector,
		   const vector<int> tailsize_vector){

  int num_folds = split_data.size();
  int rows = C_vector.size();
  int cols = tailsize_vector.size();
  double** results = new double*[rows];
  for (int i = 0; i < rows; ++i)
    results[i] = new double[cols];

  //zero accumulators
  for (int i=0;i<rows;++i)
    for (int j=0;j<cols;++j)
      results[i][j] = 0.0;

#pragma omp parallel for collapse(3)
  for (int test_fold = 0; test_fold < num_folds; ++test_fold){
    for (int i=0;i<C_vector.size();++i) {
      for (int j=0;j<tailsize_vector.size();++j) {
	results[i][j] += run_one_fold(split_data,split_labels,test_fold,num_folds,C_vector[i],tailsize_vector[j]);
      }
    }
  }

  //average accumulators
  for (int i=0;i<rows;++i)
    for (int j=0;j<cols;++j)
      results[i][j] /= num_folds;

  ofstream myfile;
  myfile.open(outFilename.c_str());
  myfile << "#C tailsize accuracy" << endl;
  for (int i=0;i<rows;++i){
    for (int j=0;j<cols;++j){
      myfile << C_vector[i] << " " << tailsize_vector[j] << " " << results[i][j] << endl;
    }
  }
  myfile.close();
  for (int i = 0; i < rows; ++i)
    delete [] results[i];
  delete [] results;
}

vector < string > get_test_filenames(string & base_path, string & extension){
  vector <string> test_files;
  for (int i=0;i<20;i++){
    ostringstream convert;
    convert << i;
    string filename  =base_path+convert.str() + "."+extension;
    test_files.push_back(filename);
  }
  return test_files;
}

vector < string > get_train_filenames(string & base_path){
  vector < string > train_files;
  for (int i=0;i<20;i++){
    ostringstream convert;
    convert << i;
    train_files.push_back(base_path + convert.str());
  }
  return train_files;
}

vector < string > get_result_filename(string & base_path,
				      string & extension){
  vector < string > result_paths;
  for ( int i=0 ; i < 20; i++){
    ostringstream convert;
    convert << i;
    result_paths.push_back(base_path + convert.str() + extension );
  }
  return result_paths;
}

int main(int argc, char ** argv){  
  if ( argc != 4) {
      cout << "you provided " << argc << " arguments; wanted 4" << endl;
      cout << "usage:  ./run_test.x <train_path> <model_path> <tailsize>";
      exit(-1);
  }

  double lambda = 1.0;
  int min_tailsize = 5;
  double margin_prob_thresh= 0.005;
  double cover_prob_thresh = 0.5;
  double sliding_window_max = 20;

  string train_filename = argv[1];
  string model_filename = argv[2];
  int max_tailsize = atoi(argv[3]);

  //get the train data
  vector < string > train_labels;
  vector < vector < double > > train_data;
  cout << "reading train data" << endl;
  readDataset(train_filename,train_labels,train_data);
  cout << "done" << endl;

  EVM * evm = new EVM(lambda,min_tailsize,max_tailsize,margin_prob_thresh, cover_prob_thresh, sliding_window_max);
  evm->fit(train_data,train_labels);
  evm->save(model_filename);
  delete evm;
  return EXIT_SUCCESS;
}
