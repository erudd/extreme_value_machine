CODE_PATH=./code/
UNAME:=$(shell uname)
ifeq ($(UNAME),Linux)
export CXXFLAGS = -Wfatal-errors -std=c++11 -g -Ofast	
else
export CXX=g++-mp-4.8 -Wfatal-errors -std=c++11 -g -Ofast	
endif
#export CXXFLAGS = -Wfatal-errors -std=c++11 -fno-inline   -g3


all: run_train_test run_train run_test
run_train_test: run_train_test.cpp $(CODE_PATH)ExtremeValueMachine.cpp $(CODE_PATH)MetaRecognition.cpp $(CODE_PATH)weibull.c
	$(CXX) $(CXXFLAGS) -I./code -I.  run_train_test.cpp $(CODE_PATH)ExtremeValueMachine.cpp -I$(CODE_PATH) $(CODE_PATH)MetaRecognition.cpp $(CODE_PATH)weibull.c -o run_train_test.x -lm -fopenmp
run_train: run_train.cpp $(CODE_PATH)ExtremeValueMachine.cpp $(CODE_PATH)MetaRecognition.cpp $(CODE_PATH)weibull.c
	$(CXX) $(CXXFLAGS)  -I./code -I.  run_train.cpp $(CODE_PATH)ExtremeValueMachine.cpp -I$(CODE_PATH) $(CODE_PATH)MetaRecognition.cpp $(CODE_PATH)weibull.c -o run_train.x -lm -fopenmp
run_test: run_test.cpp $(CODE_PATH)ExtremeValueMachine.cpp $(CODE_PATH)MetaRecognition.cpp $(CODE_PATH)weibull.c
	$(CXX) $(CXXFLAGS)  -I./code -I.  run_test.cpp $(CODE_PATH)ExtremeValueMachine.cpp -I$(CODE_PATH) $(CODE_PATH)MetaRecognition.cpp $(CODE_PATH)weibull.c -o run_test.x -lm -fopenmp
clean:	
	rm -rf *.x *.x.dSYM
