#!/usr/bin/python
from pylab import *
from numpy import *
import sys
import os

def calc_openness(ct,cr,ce):
    return 1.0 - sqrt((2.0*ct)/(cr+ce))

def get_openness(train_classes,test_classes):
    ct = len(train_classes)
    cr = len(test_classes.intersection(train_classes))
    ce = len(test_classes)
    return calc_openness(ct,cr,ce)

def calc_dynamic_thresh(openness):
    return 0.5 * openness

if __name__=='__main__':
    usage = "usage: evaluate_results.py <results filename> [-O<openset threshold>]"+ "\n" + "use <openset threshold> less than 0 for a dynamic threshold";
    if (len(sys.argv) != 2 and len(sys.argv) != 3 and  len(sys.argv) != 4) :
        print usage + str(len(sys.argv))
    thresh = 0.0
    if (len(sys.argv) >= 3):
        if "-O" not in sys.argv[2]:
            raise ValueError(usage)
        try:
            thresh = float(sys.argv[2].strip('-O'))
        except Exception as e:
            try:
                print "trying " + sys.argv[3]
                thresh = float(sys.argv[3]) 
            except Exception as e:
                raise ValueError(usage)

            
    if (thresh > 1): 
        raise ValueError("Threshold is %f; must be between 0 and 1.0"%thresh)

    with open(sys.argv[1]) as f:
        lines = f.read().splitlines()
        train_labels = set(map(lambda x: int(x),lines[0].split(': ')[1].strip(',').split(',')))
        test_labels = set(map(lambda x: int(x),lines[1].split(': ')[1].strip(',').split(',')))

    dynamic_threshold = False
    if (thresh < 0):
        dynamic_threshold = True

    if (dynamic_threshold == True):
        openness = get_openness(train_labels,test_labels)
        thresh = calc_dynamic_thresh(openness)
        sys.stderr.write("dynamic threshold = "+str(thresh))

    print "thresh%f"%(float(thresh))

    lines = [x.split(" ") for x in lines[3:]]
    predicted,actual,prob = [],[],[]

    for x in lines:
        predicted.append(int(x[0]))
        actual.append(int(x[1]))
        prob.append(float(x[2]))

    predicted = array(predicted)
    actual = array(actual)
    prob = array(prob)

    num_correct = 0
    a = where(predicted == actual)[0]
    b = where(prob >= thresh)[0]
    num_correct = len(list(set(a).intersection(set(b))))

    unknown = list(set(where(predicted != actual)[0]).intersection(set(where(prob < thresh)[0])))
    unknown = array(unknown)
    num_correct += len(unknown)

    print "Accuracy: %f%%"%(float(num_correct)/len(predicted)*100.0)


