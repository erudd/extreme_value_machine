#ifndef __EXTREMEVALUEMACHINE_HPP_INCLUDED__
#define __EXTREMEVALUEMACHINE_HPP_INCLUDED__

#include "VPTree.hpp"
#include "MetaRecognition.h"
#include <boost/algorithm/string.hpp>
#include <ctime>
#include <cstdlib>
#include <unordered_set>
#include <unordered_map>
#include <vector> 
#include <algorithm>
#include <iterator>
#include <math.h>
#include <iostream>
#include <deque>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <fstream>

#define USE_CONSTANT_TAILSIZE false
#define USE_POSITIVES_AND_NEGATIVES true
#define N_WEIBULL_PARAMS 3
#define VERBOSE true

using namespace std;

struct VPTree_Point{
  double *data;
  int size;
};

vector<string> split_line(string & line,string & delim);
double l2_distance(const VPTree_Point & p1, const VPTree_Point & p2);
double l2_vec_distance(const vector<double> &v1,const vector<double> &v2);
vector<double> retrieve_neighbor_dists(vector<double> & pos_pt,
				       VpTree<VPTree_Point,l2_distance> & tree,
				       int num_neighbors);
int zero_check(vector < double > & dists);
template <class T> int get_number_unique_values(vector<T> & vec);
template <class T> int get_first_nonzero_index(vector<T> & vec);

class EVM{
public:
  EVM(double _lambda = 1.0,
      int _min_tailsize = 5,
      int _max_tailsize = 50,
      double _margin_prob_thresh = 0.1,
      double _cover_prob_thresh = 0.5,
      int _sliding_window_max = 20);
  virtual ~EVM();
  void fit(vector < vector < double > > & data, vector <string> & labels);
  void set_NUM_MAX_AVERAGE(int n);
  unordered_map <string,vector<double> > predict_proba(vector<vector <double> > & samples);
  vector<string> predict_w_probs(vector < vector < double > > & samples, vector<double> & sample_probs);
  vector<string> predict(vector < vector < double > > & samples);
  vector <string> get_unique_class_labels();
  void save(string & path);
  void load(string & path);
  unordered_map<string,vector<MetaRecognition*> > class_Weibulls;
  unordered_map<string,vector<vector < double > > > class_points;
private:
  string DELIM = ","; // the delimeter for load and save methods
  int min_tailsize, sliding_window_max, max_tailsize;
  double margin_prob_thresh, cover_prob_thresh, lambda;
  deque<int> tailsize_sliding_window; 
  int average_tailsize;
  int NUM_MAX_AVERAGE_DEFAULT = 4;
  int NUM_MAX_AVERAGE = NUM_MAX_AVERAGE_DEFAULT;

  vector<double> get_pos_pt_dists(vector<vector<double> > & pos_pts,int IN,
				     double (*distance_func)(const vector<double>, 
							     const vector<double>));
  vector<double> get_pos_pt_dists(vector<vector<double> > & pos_pts,
				  int IN,
				  double (*distance_func)(const vector<double>&, const vector<double>&));

  unordered_set<int> set_cover(vector <MetaRecognition*> & Weibulls,
				    vector < vector <double > > & pos_pts);
  void update_average_tailsize(int item);
  int tailsize_search(MetaRecognition * WeibullCDF, vector <double> & dists, vector <double> & margin_dists);
  vector<double> get_pos_kfn_dists(vector<vector<double> >& pos_pts,int index,int num_neighbors);
  void get_Weibulls(vector< vector < double > > & pos_pts,
		    VpTree<VPTree_Point,l2_distance> & tree,
		    vector<MetaRecognition*> & chosen_Weibulls,
		    vector< vector < double > > & chosen_points);
  vector<string> getUniqueLabels(vector<string> & labels);
  void initialize_VpTree(VpTree<VPTree_Point,l2_distance> & tree, vector < vector <double > > & pts);
};
#endif
