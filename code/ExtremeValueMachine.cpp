#include <ExtremeValueMachine.hpp>

#define VERBOSE true

/**
 * Splits a string into a vector of strings from a delimeter.
 * @param string to be split
 * @param delimeter
 * @return vector of split strings
 */
vector<string> split_line(string & line,string & delim){
  vector < string > split_strs;
  boost::split(split_strs,line,boost::is_any_of(delim.c_str()));
  return split_strs;
}

/**
 * Returns the l2 distance between two points objects for the VP-Tree
 * @param
 * @param
 * @return
 */
double l2_distance(const VPTree_Point & p1, const VPTree_Point & p2){
  double dist=0.0;
  for (int i=0;i<p1.size;++i)
    dist += pow(p1.data[i]-p2.data[i],2);
  dist = sqrt(dist);
  return dist;
}

/**
 * Returns the l2 distance between two vectors
 * @param
 * @param
 * @return
 */
double l2_vec_distance(const vector<double> &v1,const vector<double> &v2){
  double dist = 0.0;
  for (int i=0;i<v1.size();++i)
    dist += pow(v1[i]-v2[i],2);
  dist = sqrt(dist);
  return dist;
}

/**
 * Returns 
 * @param positive point
 * @param VP tree of negative points
 * @param number of neighbors to retrieve
 * @return 
 */
vector<double> retrieve_neighbor_dists(vector<double> & pos_pt,
				       VpTree<VPTree_Point,l2_distance> & tree,
				       int num_neighbors){
  if (num_neighbors > tree.size()){
    num_neighbors = tree.size();
    cerr << "WARNING: Number of requested neighbors greater than number of class members." << endl;

    if (tree.size() == 0){
      cerr << "WARNING: Tree size is zero. Cannot retrieve neighbors." << endl;
    }
  }
  vector<double> distances;
  vector<VPTree_Point> neighbors;
  VPTree_Point p = {&pos_pt[0],(int)(pos_pt.size())};
  tree.search(p,num_neighbors,&neighbors,&distances);
  return distances;
}

/**
 * Checks if a vector contains any zero values
 * @param dists is a vector of distances.
 * @return 1 if no zeros found; -1 otherwise
 */
int zero_check(vector < double > & dists){
  for (int i=0;i<dists.size();++i){
    if (dists[i] == 0.0)
      return -1;
  }
  return 1;
}

/**
 * Gets the number of unique values in a vector.
 * @param templated vector of values
 * @return the number of unique values in a vector
 */
template <class T> int get_number_unique_values(vector<T> & vec){
  unordered_set<T> uniqueVals;
  for (int i=0;i<vec.size();++i)
    uniqueVals.insert(vec[i]);
  return uniqueVals.size();
}

/**
 * Find and return the first nonzero index
 * @param
 * @return
 */
template <class T> int get_first_nonzero_index(vector<T> & vec){
  int firstNonZeroIN = 0;
  for (int j=0;j<vec.size();++j){
    if (vec[j] != 0)
	break;
    ++firstNonZeroIN;
  }
  return firstNonZeroIN;
}

/**
 * Class constructor for the EVM
 * @param currently unused; functionality to be implemented in future versions
 * @param the minimum tailsize to use to start rampup. Cannot be less than 5.
 * @param the probability threshold used to define the margin. Takes a default value of 0.005
 * @param defines the cover probability
 * @param size of the sliding window over which to take averages. Used in speedup for tailsize rampup
 */
EVM::EVM(double _lambda,
	 int _min_tailsize, 
	 int _max_tailsize,
	 double _margin_prob_thresh,
	 double _cover_prob_thresh,
	 int _sliding_window_max){
  lambda = _lambda;
  if (_min_tailsize < 5){
    min_tailsize = 5;
  }else{
    min_tailsize = _min_tailsize;
  }
  average_tailsize=min_tailsize;
  margin_prob_thresh = _margin_prob_thresh;
  cover_prob_thresh = _cover_prob_thresh;
  sliding_window_max = _sliding_window_max;
  max_tailsize = _max_tailsize;
  cout << "Created EVM instance with tailsize " << max_tailsize << endl;
}

/**
 * Destructor for the EVM. Its sole purpose is to free the residual MR objects.
 */
EVM::~EVM(){
  for (unordered_map<string,vector<MetaRecognition*> >::iterator it = class_Weibulls.begin();it != class_Weibulls.end(); it++){
    for (int i=0;i<(it->second).size();i++){
      delete ((it->second)[i]);
    }
  }
  class_Weibulls.clear();
}

/**
 * Used by set cover. Returns the distance between the INth positive point and all other points.
 *
 * @param A 2D vector of feature vectors.
 * @param Index of the vector w.r.t. whcih to take distances.
 * @param A pointer to the distance function.
 * @return A vector of distances.
 */
vector<double> EVM::get_pos_pt_dists(vector<vector<double> > & pos_pts,
				     int IN,
				     double (*distance_func)(const vector<double>&, const vector<double>&)){
  vector < double > dists;
  for (int i=0; i < pos_pts.size(); ++i){
    double dist = distance_func(pos_pts[i],pos_pts[IN]);
    dists.push_back(dist);
  }
  return dists;
}


/**
 * Computes the coverage of points and Weibulls
 * @param A vector of fitted Weibulls centered about each respective element of pos_pts
 * @param the centers of the fitted Weibulls
 * @return an unordered set of indices that are covered
 */
unordered_set<int> EVM::set_cover(vector <MetaRecognition*> & Weibulls,
				  vector < vector <double > > & pos_pts){

  unordered_set <int> KeepIndices;
  int num_pos_pts = pos_pts.size();

  //allocate universe
  unordered_set<int> universe;
  for (int i=0;i<num_pos_pts;++i){
    universe.insert(i);
  }

  //get coverages
  unordered_map<int,unordered_set<int> > coverage_sets;
  for (int i=0;i<Weibulls.size();++i){
    vector < double > pos_pt_dists = get_pos_pt_dists(pos_pts,i,l2_vec_distance); 
    unordered_set<int> coverage_set;
    for (int j=0;j<pos_pt_dists.size();++j){
      if (Weibulls[i]->W_score(pos_pt_dists[j]) >= cover_prob_thresh){
	coverage_set.insert(j);
      }
    }
    coverage_sets[i] = coverage_set;
  }

  unordered_set<int> covered_elements;
  bool covered_well_enough = false;
  while((covered_elements != universe) && (!covered_well_enough) && (coverage_sets.size() > 0)){
    int biggest_set_index = 0;
    int max_elements_covered = 0;
    for (int i=0; i < coverage_sets.size(); ++i){
      unordered_set<int> difference;

      set_difference(coverage_sets[i].begin(),
		     coverage_sets[i].end(),
		     covered_elements.begin(),
		     covered_elements.end(),
		     inserter(difference,difference.end()));

      if (difference.size()  >= max_elements_covered){
	max_elements_covered = difference.size();
	biggest_set_index = i;
      }
    }
    KeepIndices.insert(biggest_set_index);
    covered_elements.insert(coverage_sets[biggest_set_index].begin(),coverage_sets[biggest_set_index].end());
    coverage_sets.erase(biggest_set_index);    
  }
  return KeepIndices;
}

/**
 * Updates the sliding window of tailsizes
 * @param the tailsize to update
 */
void EVM::update_average_tailsize(int item){
  tailsize_sliding_window.push_back(item);
  if(tailsize_sliding_window.size() > sliding_window_max){
    tailsize_sliding_window.pop_front();
  }
  int avg = 0;
  for (auto elem : tailsize_sliding_window)
    avg += elem;
  avg /= tailsize_sliding_window.size();
  average_tailsize = avg;
}

/**
 * Used to ramp up the tailsize for the EVM. Called if using non-constant tailsize.
 * TODO: In next version: more graceful treatment of bad fit return codes
 * @param pointer to the MR instance
 * @param vector of distances
 * @param vector of margin distances
 * @return the selected tailsize
 */
int EVM::tailsize_search(MetaRecognition * WeibullCDF, vector <double> & dists, vector <double> & margin_dists){
  int min_of_max_tailsize = dists.size();
  int tailsize,old_tailsize;
  tailsize=average_tailsize;
  //ramp up the tailsize exponentially
  for(;;){
    old_tailsize = tailsize;
    tailsize *= 2;
    if(tailsize >= margin_dists.size())
      break;

    if (WeibullCDF->FitLowEVM(&margin_dists[0],margin_dists.size(),tailsize) != 1){
      cerr << "WARNING: Bad error code from Weibull fit." << endl;
    }

    if (WeibullCDF->W_score(dists[0]) > margin_prob_thresh)
      break;
  }
  bool found = false;
  if (tailsize >= min_of_max_tailsize){
    tailsize = min_of_max_tailsize;

    if (WeibullCDF->FitLowEVM(&margin_dists[0],margin_dists.size(),tailsize) != 1){
      cerr << "WARNINING: bad return code in tailsize search!" << endl;
    }

    if (WeibullCDF->W_score(dists[0]) <= margin_prob_thresh){
      found = true;
    }
  }
  if (!found){
      int lo = old_tailsize;
      int hi = tailsize;
      int mid;
      double cdf_eval = 0.0;
      //binary search for the "optimal" tailsize
      tailsize = (lo+hi) / 2;
      if (tailsize > margin_dists.size()){
	WeibullCDF->FitLowEVM(&margin_dists[0],margin_dists.size(),margin_dists.size()); 
	cdf_eval = WeibullCDF->W_score(dists[0]);
	if (cdf_eval < margin_prob_thresh){
	  tailsize = margin_dists.size();
	  //jump past the while loop
	  goto exit_fxn;
	}else{
	  hi = margin_dists.size();
	}
      }
      while ((lo < hi) && (!found)){
	tailsize = (lo+hi)/2;
	if (WeibullCDF->FitLowEVM(&margin_dists[0],margin_dists.size(),tailsize) != 1){
	  cerr << "WARNING: Bad error code in tailsize search." << endl;
	}
	if (tailsize > dists.size()){
	  hi = tailsize-1;
	}
	cdf_eval = WeibullCDF->W_score(dists[0]);
	if (cdf_eval == margin_prob_thresh){
	  found = true;
	}else{
	  if(cdf_eval > margin_prob_thresh){
	    hi = tailsize - 1;
	  }else if (cdf_eval < margin_prob_thresh){
	    lo = tailsize + 1;
	  }
	}
      }
      if (cdf_eval > margin_prob_thresh){
	tailsize--;
      }
  }
 exit_fxn:
  update_average_tailsize(tailsize);
  return tailsize;
}

/**
 * Gets the distances of the k farthest neighbors within a class
 * @param vector of positive points
 * @param the index of the point to compare
 * @param the number of neighbors to return
 * @return vector of distances
 */
vector<double> EVM::get_pos_kfn_dists(vector<vector<double> >& pos_pts,int index,int num_neighbors){
  vector <double> dists(pos_pts.size() - 1);
  for (int i=0,j=0;i<pos_pts.size();++i){
    if (i != index)
      dists[j++] = l2_vec_distance(pos_pts[index],pos_pts[i]);
  }
  partial_sort(dists.begin(),dists.begin()+num_neighbors,dists.end(),greater<double>());
  vector<double> kfn_dists(dists.begin(),dists.begin()+num_neighbors);
  return kfn_dists;
}

/**
 * Given positive points and a VP-tree of negative points, this function gets all
 * Weibulls that characterize the class as well as their corresponding center points.
 * Generalization is performed via the greedy set cover approximation.
 *
 * @param a 2D vector of positive points.
 * @param a VP-tree of negative points.
 * @parama a vector of MR objects to be filled by this function.
 * @param a 2D vector of points to be filled by this function. 
 */
void EVM::get_Weibulls(vector< vector < double > > & pos_pts,
		       VpTree<VPTree_Point,l2_distance> & tree,
		       vector<MetaRecognition*> & chosen_Weibulls,
		       vector< vector < double > > & chosen_points){
  vector<MetaRecognition*> Weibulls;
  Weibulls.reserve(pos_pts.size());
  
  // if max tailsize exceeds half the data then use half the data instead of the max tailsize
  int half_negative_data_size = tree.size() / 2;
  int min_of_max_tailsize = min(max_tailsize,half_negative_data_size);

  if (min_of_max_tailsize > half_negative_data_size){
    cerr << "WARNING: Tailsize " << max_tailsize << " exceeds half of the data. " << endl;
    cerr << "\t Defaulting to " << min_of_max_tailsize << endl;
  }

  for (int i=0;i<pos_pts.size();++i){
    MetaRecognition * WeibullCDF = new MetaRecognition();
    int chosen_tailsize;

    //try to retreive the nearest negative neighbors from the VP tree    
    vector<double> dists = retrieve_neighbor_dists(pos_pts[i],tree,min_of_max_tailsize);
    
    //if there are non-zero distances, remove them and check that dists is of sufficient size
    int firstNonZeroIN = get_first_nonzero_index(dists);
    if (firstNonZeroIN != 0){
      vector <double> temp(dists.begin()+firstNonZeroIN,dists.end());
      dists.clear();
      dists = temp;
      if (dists.size() < min_tailsize){
	//return default parameters
	WeibullCDF->set_params(1./((double)pos_pts[i].size()),2.0);
	Weibulls.push_back(WeibullCDF);
	continue;
      }
    }
    
    //check that we have enough unique values; if not pick the default Weibull parameters
    if (get_number_unique_values(dists) < 3){
	//default parameters
	WeibullCDF->set_params(1./((double)pos_pts[i].size()),2.0);
	Weibulls.push_back(WeibullCDF);
	continue;
    }

    vector<double> margin_dists = dists;
    for (int j=0;j<margin_dists.size();++j){
      margin_dists[j] /= 2.0;
    }
    
    if (USE_POSITIVES_AND_NEGATIVES){
      int tailsize = 0;
      if (USE_CONSTANT_TAILSIZE == false){
	tailsize = tailsize_search(WeibullCDF,dists,margin_dists);
      }else if (USE_CONSTANT_TAILSIZE == true){
	tailsize = min_of_max_tailsize;
      }
      if (tailsize > (pos_pts.size() - 1)){
	tailsize = pos_pts.size() - 1;
	cout << "DANGER : TAILSIZE >= THE NUMBER OF POSITIVE POINTS" << endl;
	cout << "    Retreiving num pos points minus 1 but classes are wildly imbalanced" << endl;
      }
      vector<double> pos_dists = get_pos_kfn_dists(pos_pts,i,tailsize);
      if (zero_check(pos_dists) == -1){
	cerr << "Warning: 0 distance of farthest positive point. Returning default parameters" << endl;
	WeibullCDF->set_params(1./((double)pos_pts[i].size()),2.0);
	Weibulls.push_back(WeibullCDF);
	continue;
      }
      //scale the positive dists
      for (int j=0;j<pos_dists.size();j++){
	pos_dists[j] *= lambda;
      }
      vector<double> pos_and_margin_dists;
      merge(pos_dists.begin(),pos_dists.end(),margin_dists.begin(),margin_dists.end(),back_inserter(pos_and_margin_dists));
      //now fit on the merge between the positive distance outliers and the margin distances for the point in question
      if (WeibullCDF->FitLowEVM(&pos_and_margin_dists[0],pos_and_margin_dists.size(),tailsize) != 1){
	  cerr << "WARNING: Bad return code in Weibull fit. Returning default parameters " << endl;
	  WeibullCDF->set_params(1./((double)pos_pts[i].size()),2.0);
	  Weibulls.push_back(WeibullCDF);
	  continue;
      }

    }else{
      if (USE_CONSTANT_TAILSIZE == false){
	int tailsize = tailsize_search(WeibullCDF,dists,margin_dists);
      }else if (USE_CONSTANT_TAILSIZE == true){
	if (WeibullCDF->FitLowEVM(&margin_dists[0],margin_dists.size(),min_of_max_tailsize != 1)){
	  cerr << "WARNING: Bad return code in Weibull fit. Returning default parameters." << endl;
	  WeibullCDF->set_params(1./((double)pos_pts[i].size()),2.0);
	  Weibulls.push_back(WeibullCDF);
	  continue;
	}
      }
    }
    Weibulls.push_back(WeibullCDF);
  }
  unordered_set < int > IN = set_cover(Weibulls,pos_pts);
  for (int i=0;i<Weibulls.size();++i){
    if (IN.count(i) == 1){ 
      chosen_points.push_back(pos_pts[i]);
      chosen_Weibulls.push_back(Weibulls[i]);
    }else{
      delete Weibulls[i];
    }
    Weibulls[i] = NULL;
  }
}

/**
 * Returns a vector of unique labels from a vector of labels
 *
 *@param
 *@return
 */
vector<string> EVM::getUniqueLabels(vector<string> & labels){
  unordered_set<string> uniqueLabelSet;
  for (int i=0;i<labels.size();i++){
    uniqueLabelSet.insert(labels[i]);
  }
  vector<string> uniqueLabels;
  for (auto i = uniqueLabelSet.begin(); i != uniqueLabelSet.end(); ++i) {
    uniqueLabels.push_back(*i);
  }
  sort(uniqueLabels.begin(),uniqueLabels.end());
  return uniqueLabels;
}

/**
 * Initializes a VP-tree from a 2D vector of points
 *
 * @param a templated VP-tree instance
 * @param a 2D vector of points
 */
void EVM::initialize_VpTree(VpTree<VPTree_Point,l2_distance> & tree, vector < vector <double > > & pts){
  vector<VPTree_Point> points;
  for (int i=0;i<pts.size();i++){
    VPTree_Point p = {&pts[i][0],(int)(pts[i].size())};
    points.push_back(p);
  }
  tree.create(points);
}

/**
 * Fit an EVM instance -- data to labels ala sklearn
 *
 * @param 2D vector of data
 * @param 1D vector of labels
 */
void EVM::fit(vector < vector < double > > & data, vector <string> & labels){
  unordered_map < string, vector < vector < double > > > class_data_by_label;
  vector<string> uniqueLabels = getUniqueLabels(labels);
  for (int i=0;i<uniqueLabels.size();++i){
#if (VERBOSE == true)    
    cerr << "fitting " << uniqueLabels[i] << endl;
#endif
    vector<vector <double> > pos_pts;
    vector<vector <double> > neg_pts;
    for (int j=0;j<labels.size();++j){
      if (labels[j].compare(uniqueLabels[i]) == 0){
	pos_pts.push_back(data[j]);
      }else{
	neg_pts.push_back(data[j]);
      }
    }
    VpTree<VPTree_Point,l2_distance> * tree = new VpTree<VPTree_Point,l2_distance>();
    initialize_VpTree(*tree,neg_pts);
    vector <MetaRecognition*> Weibulls;
    vector <vector<double> > chosen_pts;
    class_data_by_label[uniqueLabels[i]] = pos_pts;
    get_Weibulls(pos_pts,*tree,Weibulls,chosen_pts);
    class_Weibulls[uniqueLabels[i]] = Weibulls;
    class_points[uniqueLabels[i]] = chosen_pts;
    delete tree;
  }
}

/**
 * For dynamically setting the number of Weibulls over which to average. Note that this should be called only once for any EVM instance
 * @param Number of maxima over which to average
 */
void EVM::set_NUM_MAX_AVERAGE(int n){
  if (n <= 0){
    cout << "bad value "<< n << " to set_NUM_MAX_AVERAGE; Defaulting to " << NUM_MAX_AVERAGE_DEFAULT;
    NUM_MAX_AVERAGE = NUM_MAX_AVERAGE_DEFAULT;
  }else{
    NUM_MAX_AVERAGE = n;
  }
}

/**
 * Predicts probabilities that samples are associated with each class.
 *
 * @param a vector of samples over which to compute probabilities
 * @return a unordered_map keyed on class name with a vector of probabilities (corresponding to respective samples)
 */
unordered_map <string,vector<double> > EVM::predict_proba(vector<vector <double> > & samples){
  vector<string> keys;
  for (unordered_map<string,vector<MetaRecognition*>>::iterator it = class_Weibulls.begin(); it != class_Weibulls.end(); it++){
    keys.push_back((string)(it->first));
  }

  unordered_map<string,vector<double> > return_probs;
  for (int i=0; i<samples.size(); ++i){
    for (auto key : keys){
      if (return_probs.find(key) == return_probs.end()){
	vector<double> newvec(samples.size());
	return_probs[key] = newvec;
      }

      vector<double> prob_vec(class_Weibulls[key].size());
      for (int j=0; j<class_Weibulls[key].size();++j){
	double dist = l2_vec_distance(class_points[key][j],samples[i]);
	prob_vec[j] = class_Weibulls[key][j]->W_score(dist); 
      }

      // this error checking is important because set cover may cover the class in fewer than NUM_MAX_AVERAGE_WEIBULLS
      int max_sorted_index = min((int)NUM_MAX_AVERAGE,(int)prob_vec.size());
      partial_sort(prob_vec.begin(),prob_vec.begin()+max_sorted_index,prob_vec.end(),greater<double>());
      double avg_prob = 0.0;
      for (int j=0;j<max_sorted_index;++j){
	avg_prob += prob_vec[j];
      }
      return_probs[key][i] = avg_prob/max_sorted_index;
    }
  }
  return return_probs;
}

/**
 * Given a vector of samples, returns the predicted classes and the associated probabilities for the top-scoreing classes.
 * The probability is included for use in open-set recognition. 
 *
 * @param a 2D vector of samples
 * @param a vector of sample probabilities to be filled. IMPORTANT: SHOULD BE PREALLOCATED TO SIZE OF SAMPLES
 * AND FILLED W/ ZEROS WHEN PASSED
 * @return predicted class labels
 */
vector<string> EVM::predict_w_probs(vector < vector < double > > & samples, vector<double> & sample_probs){
  vector <string> results;
  unordered_map<string,vector<double> > probs = predict_proba(samples);
  for (int i=0;i<samples.size();i++){
    string max_label="None";
    double max_value=-1.0;
    for (unordered_map<string,vector<double> >::iterator it = probs.begin();it != probs.end(); ++it){
      if ((it->second)[i] > max_value){
	max_value = (it->second)[i];
	max_label = it->first;
      }
    }
    results.push_back(max_label);
    sample_probs[i] = max_value;
  }
  return results;
}

/**
 * Given a vector of samples, returns the predicted classes. Associated probabilities are not returned.
 * Hence, this function should only be used in a closed-set regime.
 *
 * @param a 2D vector of samples
 * @return predicted class labels
 */
vector<string> EVM::predict(vector < vector < double > > & samples){
  vector <string> results;
  unordered_map<string,vector<double> > probs = predict_proba(samples);
  for (int i=0;i<samples.size();i++){
    string max_label="None";
    double max_value=-1.0;
    for (unordered_map<string,vector<double> >::iterator it = probs.begin();it != probs.end(); it++){
      if ((it->second)[i] > max_value){
	max_value = (it->second)[i];
	max_label = it->first;
      }
    }
    results.push_back(max_label);
  }
  return results;
}

/**
 * Returns a vector of unique class labels that the evm contains
 * @return a vector of unique class labels
 */
vector <string> EVM::get_unique_class_labels(){
  vector <string> uniqueLabels;
  uniqueLabels.reserve(class_Weibulls.size());
  for (unordered_map<string,vector<MetaRecognition*> >::iterator it=class_Weibulls.begin();it!= class_Weibulls.end();++it){
    uniqueLabels.push_back(it->first);
  }
  return uniqueLabels;
}

/**
 * Saves the EVM model to disk.
 * @param full path to file
 */
void EVM::save(string & path){
#if (VERBOSE == true)
  cout << "serializing EVM to disk: " << path << endl;
#endif
  ofstream myfile;
  myfile.precision(25);
  myfile.open(path.c_str());
  //integer values
  myfile << "# int min_tailsize, sliding_window_max, max_tailsize;" << endl;
  myfile << min_tailsize << DELIM << sliding_window_max << DELIM << max_tailsize << endl;
  //double values
  myfile << "# double margin_prob_thresh, cover_prob_thresh, lambda;" << endl;
  myfile << margin_prob_thresh << DELIM << cover_prob_thresh << DELIM << lambda << endl;
  // Weibull params and points
  myfile << "# class scale shape point[0], point[1], ..." << endl;
  for (unordered_map<string,vector<MetaRecognition*> >::iterator it=class_Weibulls.begin();it!= class_Weibulls.end();++it){
    string key = it->first;
    unordered_map<string,vector<MetaRecognition*> >::iterator tempIT = it;
    ++tempIT;
    for ( int i=0;i<class_Weibulls[key].size();++i){
      myfile << key << DELIM;
      myfile << class_Weibulls[key][i]->get_scale() << DELIM;
      myfile << class_Weibulls[key][i]->get_shape() << DELIM;
      myfile << class_Weibulls[key][i]->get_small_score() << DELIM;
      myfile << class_Weibulls[key][i]->get_translate_amount() << DELIM;
      for (int j=0;j<class_points[key][i].size();++j){
	if (j != class_points[key][i].size()-1)
	  myfile << class_points[key][i][j] << DELIM;
	else
	  myfile << class_points[key][i][j];
      }
      if (tempIT != class_Weibulls.end()){
	myfile << endl;
      }else if (i != class_Weibulls[key].size() -1) {
	myfile << endl;
      }
    }
  }
  myfile.close();
}

/**
 * Loads an EVM model from disk
 * @param full path to file
 */
void EVM::load(string & path){
#if (VERBOSE == true)
  cout << "loading model from disk: " << path << endl;
#endif
  ifstream myfile(path.c_str());
  myfile.precision(20);
  string line;
  if (myfile.is_open()){
    //read the first line (just a comment)
    getline(myfile,line);
    getline(myfile,line);

    //process the line 
    vector <string> split = split_line(line,DELIM);
    min_tailsize = atoi(split[0].c_str());
    sliding_window_max = atoi(split[1].c_str());
    max_tailsize = atoi(split[2].c_str());

    //read the 3rd line (just a comment)
    getline(myfile,line);
    getline(myfile,line);

    //process the line 
    split = split_line(line,DELIM);
    margin_prob_thresh = atof(split[0].c_str());
    cover_prob_thresh = atof(split[1].c_str());
    lambda = atof(split[2].c_str());

    //clear out class_Weibulls and points    
    for (unordered_map<string,vector<MetaRecognition*> >::iterator it=class_Weibulls.begin();it!= class_Weibulls.end();++it){
      if (it->second.size() != 0){
	cerr << "clearing out Weibulls for class " << it->first << endl;
	for (int i=0;it->second.size();++i){
	  delete it->second[i];
	}
      }
    }
    class_Weibulls.clear();
    class_points.clear();

    getline(myfile,line);
    while(getline(myfile,line)){
      split = split_line(line,DELIM);
      string _class= split[0];
      double scale = atof(split[1].c_str());
      double shape = atof(split[2].c_str());
      double smalls = atof(split[3].c_str());
      double tamount = atof(split[4].c_str());

      //check if class_Weibulls has the key
      if (class_Weibulls.find(_class) == class_Weibulls.end()){
	vector <MetaRecognition *> temp;
	class_Weibulls[_class] = temp;
      }

      //spawn a Weibull
      MetaRecognition * WeibullCDF  = new MetaRecognition();
      WeibullCDF->set_params(scale,shape);            
      WeibullCDF->set_sign(-1);
      WeibullCDF->set_small_score(smalls);
      WeibullCDF->set_translate_amount(tamount);
      WeibullCDF->set_valid();
      class_Weibulls[_class].push_back(WeibullCDF);

      vector <double> point;
      point.reserve(split.size() - 3);
      for (int i=5; i< split.size(); ++i){
	point.push_back(atof(split[i].c_str()));
      }

      if (class_points.find(_class) == class_points.end()){
	vector < vector < double > > temp;
	class_points[_class] = temp;
      }
      class_points[_class].push_back(point);
    }
    cout << "Successfully loaded model from file." << endl;
    myfile.close();
  }else{
    cerr << "ERROR: Unable to load from file " << path << "." << endl;
  }
}
